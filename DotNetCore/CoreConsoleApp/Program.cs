﻿using System;
using Library;

namespace CoreConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            String message = "DotNetCore";
            new Library.Automation().printTheMessage(message);
            Console.ReadKey();
        }
    }
}
