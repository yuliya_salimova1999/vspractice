﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            String message = "QA Automation";
            new Library.Automation().printTheMessage(message);
            Console.ReadKey();
        }
    }
}
